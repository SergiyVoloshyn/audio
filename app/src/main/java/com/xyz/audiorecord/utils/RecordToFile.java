package com.xyz.audiorecord.utils;

import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;

import com.xyz.audiorecord.model.AudioData;

import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.concurrent.BlockingQueue;

import static android.media.AudioFormat.ENCODING_PCM_16BIT;
import static com.xyz.audiorecord.MainActivity.SAMPLE_RATE;
import static com.xyz.audiorecord.MainActivity.isRecording;
import static com.xyz.audiorecord.utils.Util.minInternalBufferSize;

public class RecordToFile {

    public static class ProducerRecordFromMic implements Runnable {

        private BlockingQueue<AudioData> audioFromMic;

        public ProducerRecordFromMic(BlockingQueue<AudioData> d) {
            this.audioFromMic = d;
        }

        public void run() {
            AudioRecord audioRecord = null;
            try {
                int internalBufferSize = minInternalBufferSize * 4;

                audioRecord = new AudioRecord(MediaRecorder.AudioSource.MIC,
                        SAMPLE_RATE, AudioFormat.CHANNEL_IN_MONO, ENCODING_PCM_16BIT, internalBufferSize);

                short[] audioDataRead = new short[minInternalBufferSize];

                audioRecord.startRecording();
                while (isRecording) {
                    int numberOfShort = audioRecord.read(audioDataRead, 0, minInternalBufferSize);

                    AudioData audioDataReadClass = new AudioData();
                    for (int i = 0; i < numberOfShort; i++) {
                        audioDataReadClass.data[i] = audioDataRead[i];
                    }
                    audioFromMic.add(audioDataReadClass);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (audioRecord != null) {
                audioRecord.stop();
            }
        }
    }

    public static class ConsumerRecordFromMic implements Runnable {
        private BlockingQueue<AudioData> audioFromMic;
        File audioFile;

        public ConsumerRecordFromMic(BlockingQueue<AudioData> d, File audioFile) {
            this.audioFromMic = d;
            this.audioFile = audioFile;
        }

        public void run() {
            try {
                OutputStream outputStream = new FileOutputStream(audioFile);
                BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(outputStream);
                DataOutputStream dataOutputStream = new DataOutputStream(bufferedOutputStream);

                AudioData audioDataReadClass = new AudioData();
                while (isRecording) {
                    audioDataReadClass = audioFromMic.take();

                    for (int i = 0; i < audioDataReadClass.data.length; i++) {
                        dataOutputStream.writeShort(audioDataReadClass.data[i]);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
