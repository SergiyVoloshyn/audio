package com.xyz.audiorecord.utils;

import android.media.AudioAttributes;
import android.media.AudioFormat;
import android.media.AudioTrack;

import com.xyz.audiorecord.model.AudioData;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.concurrent.BlockingQueue;

import static com.xyz.audiorecord.MainActivity.SAMPLE_RATE;
import static com.xyz.audiorecord.MainActivity.isPlaying;
import static com.xyz.audiorecord.utils.Util.minInternalBufferSize;

public class PlayFromFile {

    public static class ProducerPlayFromFile implements Runnable {

        private BlockingQueue<AudioData> audioFromFile;
        File recordedAudioFile;

        public ProducerPlayFromFile(BlockingQueue<AudioData> d, File recordedAudioFile) {
            this.audioFromFile = d;
            this.recordedAudioFile = recordedAudioFile;
        }

        public void run() {

            RandomAccessFile randomAccessFile = null;
            try {
                randomAccessFile = new RandomAccessFile(recordedAudioFile, "r");
                long fileLength = recordedAudioFile.length() - 2;
                randomAccessFile.seek(fileLength);

                long pointer = fileLength;

                finalLabel:
                while (pointer >= 0) {
                    AudioData audioDataReadClass = new AudioData();

                    for (int i = 0; i < audioDataReadClass.data.length; i++) {
                        audioDataReadClass.data[i] = randomAccessFile.readShort();
                        pointer -= 2;
                        if (pointer >= 0) {
                            randomAccessFile.seek(pointer);
                        } else {
                            audioFromFile.put(audioDataReadClass);
                            break finalLabel;
                        }
                    }
                    audioFromFile.put(audioDataReadClass);
                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (randomAccessFile != null) {
                    try {
                        randomAccessFile.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    public static class ConsumerPlayFromFile implements Runnable {
        private BlockingQueue<AudioData> audioFromFile;

        public ConsumerPlayFromFile(BlockingQueue<AudioData> d) {
            this.audioFromFile = d;

        }

        public void run() {
            //create audio player
            AudioTrack player = new AudioTrack.Builder()
                    .setAudioAttributes(new AudioAttributes.Builder()
                            .setUsage(AudioAttributes.USAGE_ALARM)
                            .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                            .build())
                    .setAudioFormat(new AudioFormat.Builder()
                            .setEncoding(AudioFormat.ENCODING_PCM_16BIT)
                            .setSampleRate(SAMPLE_RATE)
                            .setChannelMask(AudioFormat.CHANNEL_OUT_MONO)
                            .build())
                    .setBufferSizeInBytes(minInternalBufferSize)
                    .build();
            player.play();
            //get audio data and play it
            try {
                AudioData audioData = new AudioData();
                while (isPlaying) {
                    audioData = audioFromFile.take();
                    player.write(audioData.data, 0, minInternalBufferSize);
                }
                player.stop();
                player.release();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


}
