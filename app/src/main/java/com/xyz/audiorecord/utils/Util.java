package com.xyz.audiorecord.utils;

import android.content.Context;
import android.content.pm.PackageManager;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import static android.media.AudioFormat.ENCODING_PCM_16BIT;
import static com.xyz.audiorecord.MainActivity.SAMPLE_RATE;


public class Util {

    public static int minInternalBufferSize = AudioRecord.getMinBufferSize(SAMPLE_RATE,
            AudioFormat.CHANNEL_IN_MONO, ENCODING_PCM_16BIT);

    // afterDelay will be executed after X milliseconds.
    public interface DelayCallback {
        void afterDelay();
    }

    public static void delay(int msecs, final DelayCallback delayCallback) {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                delayCallback.afterDelay();
            }
        }, msecs);
    }

    // Create an audio file name
    public static File createAudioFile(Context context) {
        String timeStamp = new SimpleDateFormat("ddMM_Y_HHmmss").format(new Date());
        String audioFileName = "audio_" + timeStamp;
        File storageDir = context.getExternalFilesDir(Environment.DIRECTORY_MUSIC);
        File audio = null;
        try {
            audio = File.createTempFile(
                    audioFileName,
                    ".pcm",
                    storageDir
            );
        } catch (Exception e) {
            e.printStackTrace();
        }
        return audio;
    }

    //Check permissions
    public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }
}


