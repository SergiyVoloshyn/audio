package com.xyz.audiorecord;

import android.Manifest;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.Toast;

import com.xyz.audiorecord.model.AudioData;
import com.xyz.audiorecord.utils.PlayFromFile;
import com.xyz.audiorecord.utils.RecordToFile;
import com.xyz.audiorecord.utils.Util;

import java.io.File;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.xyz.audiorecord.utils.Util.createAudioFile;
import static com.xyz.audiorecord.utils.Util.hasPermissions;
/*
--------------------
написать тестовое приложение захвата аудио

функциональность
- кнопка записать записывает 10 секунд данных с микрофона в файл
- кнопка воспроизвести воспроизводит файл в обратном порядке

требования
- использовать Android Java API
- запись и воспроизведение должны быть реализованы с помощью многопоточной схемы производитель/потребитель для
 избежания блокировок аудио устройства

С уважением,
Харечко Илона
компания TrueConf

Контактное лицо: Илона Харечко
e-mail: kharechko@trueconf.ru
 */
public class MainActivity extends AppCompatActivity {

    public static final int SAMPLE_RATE = 22050;
    public static final int DURATION = 10000;

    int PERMISSION_ALL = 1;
    public static boolean isRecording;
    public static boolean isPlaying;
    boolean permissionsOk = false;
    File recordedAudio;

    String[] PERMISSIONS = {
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.RECORD_AUDIO
    };

    @BindView(R.id.button_record)
    Button recordButton;

    @BindView(R.id.button_reverse_play)
    Button playReverseButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);
        checkPermissions();
    }
    @OnClick(R.id.button_reverse_play)
    void reversePlay() {
        //if file not recorded exit
        if (recordedAudio == null) {
            return;
        }
        //set timer for playing
        playReverseButton.setEnabled(false);
        Util.delay(DURATION, new Util.DelayCallback() {
            @Override
            public void afterDelay() {
                isPlaying = false;
                playReverseButton.setEnabled(true);
            }
        });
        //else play it
        isPlaying = true;
        playReverseButton.setEnabled(false);

        BlockingQueue<AudioData> audioFromFile = new ArrayBlockingQueue<AudioData>(100, true);
        (new Thread(new PlayFromFile.ProducerPlayFromFile(audioFromFile, recordedAudio))).start();
        (new Thread(new PlayFromFile.ConsumerPlayFromFile(audioFromFile))).start();

        Toast.makeText(MainActivity.this, getResources().getString(R.string.button_play_reverse),
                Toast.LENGTH_LONG).show();
    }
    @OnClick(R.id.button_record)
    void record() {
        //check permissions
        if (!permissionsOk) {
            checkPermissions();
            return;
        }
        //set timer for recording
        try {
            recordButton.setEnabled(false);
            Util.delay(DURATION, new Util.DelayCallback() {
                @Override
                public void afterDelay() {
                    isRecording = false;
                    recordButton.setEnabled(true);
                }
            });

            isRecording = true;
            recordedAudio = createAudioFile(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //start isRecording
        BlockingQueue<AudioData> audioFromMic = new ArrayBlockingQueue<AudioData>(100, true);
        (new Thread(new RecordToFile.ProducerRecordFromMic(audioFromMic))).start();
        (new Thread(new RecordToFile.ConsumerRecordFromMic(audioFromMic, recordedAudio))).start();

        Toast.makeText(MainActivity.this, getResources().getString(R.string.button_record),
                Toast.LENGTH_LONG).show();
    }

    void checkPermissions() {
        if (!hasPermissions(this, PERMISSIONS)) {
            requestPermissions(PERMISSIONS, PERMISSION_ALL);
            permissionsOk = false;
        }
        permissionsOk = true;
    }
}

