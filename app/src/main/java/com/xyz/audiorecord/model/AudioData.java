package com.xyz.audiorecord.model;

import static com.xyz.audiorecord.utils.Util.minInternalBufferSize;


public class AudioData {
    public short[] data = new short[minInternalBufferSize];
}
